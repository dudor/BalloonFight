﻿using UnityEngine;
using System.Collections;

public class testHeroCtrl1 : MonoBehaviour {

    public int HP = 3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void BallonCollisionEnter(GameObject o)
    {
        HP -= 1;
        o.SetActive(false);
    }

    public void OnPlayerDie(GameConfig.ObstructionModel model) 
    {
    
        switch (model)
        {
            case GameConfig.ObstructionModel.OUTREEL_DIE:
                Die(transform);
                break;
            case GameConfig.ObstructionModel.DRAPSEA_DIE:
                Die(transform);
                break;
            default:
                break;
        }
    }

    private void Die(Transform t) 
    {
        Transform tempT = null;

        for (int i = 1; i <= (int)GameConfig.MAX_BALLON; i++)
        {
            tempT = t.FindChild(string.Format("Ballon{0}", i));
            if (tempT)
            {
                tempT.gameObject.SetActive(false);
            }
        }
    }



}