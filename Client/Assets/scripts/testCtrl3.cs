﻿using UnityEngine;
using System.Collections;

public class testCtrl3 : MonoBehaviour
{
    public Transform Hero;
    private bool MoveState { get; set; }
    private Direction DirHR { get; set; }
    private Direction DirVH { get; set; }

    private float drag;

    void Start()
    {
        Bpc = new BasePropertiyConfig
        {
            HRDrag = 59.2f,
            VRDrag = 1.65f,
            UpForce = 68.63f,
            Mass = 22.08f
        };


        drag = Hero.rigidbody2D.drag;
        Hero.rigidbody2D.mass = Bpc.Mass;

        DirVH = Direction.DOWN;
        DirHR = Direction.DOWN;
    }

    BasePropertiyConfig Bpc;


    public void OnPress(GameObject o)
    {
        Hero.rigidbody2D.drag = drag;

        MoveState = true;
        switch (o.name)
        {
            case "top":
                DirVH = Direction.UP;
                break;
            case "down":

                break;
            case "left":
                DirHR = Direction.LEFT;
                break;
            case "right":
                DirHR = Direction.RIGHT;
                break;
            default:
                break;
        }
    }

    public void OnRelease(GameObject o)
    {
        Hero.rigidbody2D.drag = Bpc.VRDrag;


        switch (o.name)
        {
            case "top":
                DirVH = Direction.DOWN;
                break;
            case "down":

                break;
            case "left":
                DirHR = Direction.DOWN;
                break;
            case "right":
                DirHR = Direction.DOWN;
                break;
            default:
                break;
        }

        if (DirHR == Direction.DOWN && DirVH == Direction.DOWN)
        {
            MoveState = false;
        }
    }

    private void SwitchDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.LEFT:
                if (Hero.localScale.x == -1)
                {
                    Hero.localScale = new Vector3(1, 1, 1);
                }
                break;
            case Direction.RIGHT:
                if (Hero.localScale.x == 1)
                {
                    Hero.localScale = new Vector3(-1, 1, 1);
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (MoveState)
        {
            SwitchDirection(DirHR);

            if (DirHR == Direction.DOWN)
            {
                if (DirVH != Direction.DOWN)
                {
                    Hero.rigidbody2D.AddForce(new Vector2(0, 1) * Bpc.UpForce);
                }
            }
            else
            {
                if (DirVH != Direction.DOWN)
                {
                    switch (DirHR)
                    {
                        case Direction.UP:
                            break;
                        case Direction.DOWN:
                            break;
                        case Direction.LEFT:

                            Hero.rigidbody2D.AddForce(new Vector2(-1, 1) * Bpc.UpForce);
                            break;
                        case Direction.RIGHT:
                            Hero.rigidbody2D.AddForce(new Vector2(1, 1) * Bpc.UpForce);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (DirHR)
                    {
                        case Direction.UP:
                            break;
                        case Direction.DOWN:
                            break;
                        case Direction.LEFT:
                            Hero.rigidbody2D.AddForce(new Vector2(-1, 0) * Bpc.HRDrag);
                            break;
                        case Direction.RIGHT:
                            Hero.rigidbody2D.AddForce(new Vector2(1, 0) * Bpc.HRDrag);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}



public class BasePropertiyConfig
{
    public float UpForce { get; set; }
    public float HRDrag { get; set; }
    public float VRDrag { get; set; }
    public float Mass { get; set; }
}
