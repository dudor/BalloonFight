﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestory:MonoBehaviour
{
    public float distance;

    public void Update()
    {
        if (transform.localPosition.x < distance)
        {
            Destroy(gameObject);
        }
    }


}