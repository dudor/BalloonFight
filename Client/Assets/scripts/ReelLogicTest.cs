﻿using UnityEngine;
using System.Collections;

public class ReelLogicTest : MonoBehaviour {

    private float distance;

    void Start()
    {
        GameConfig.REEL_STATE = true;
        rigidbody2D.velocity = new Vector2(GameConfig.REEL_SPEED ,0);
        distance = (GameConfig.TestPage- 1) * GameConfig.BackgroundImageWidth;
    }


    void Update() 
    {
        if (transform.localPosition.x>=distance)
        {
            GameConfig.REEL_STATE = false;
            rigidbody2D.velocity = Vector2.zero;
        }
    }




}
