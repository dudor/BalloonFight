﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CoreLogicCtrl : MonoBehaviour
{
    private static CoreLogicCtrl instance;

    public GameObject Boss;
    public Slider HP;

    void Awake() 
    {
        instance = this;    
    }

    public static CoreLogicCtrl Instance {
        get
        {
            if (!instance)
            {
                instance = new CoreLogicCtrl();
            }
            return instance;
        } 
    }


    public void ShowBoss() 
    {
        RunActionDelay(5f, () => { Boss.SetActive(true); });
    }


    public void RunActionDelay(float time, Action ac)
    {
        StartCoroutine(DelayRun(time, ac));
    }

    IEnumerator DelayRun(float time, Action ac)
    {
        yield return new WaitForSeconds(time);
        if (ac != null)
        {
            ac.Invoke();
        }
    }

    public void AttackUser(float e) 
    {
        HP.value -= .2f;
    }
}