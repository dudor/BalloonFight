﻿using UnityEngine;
using System.Collections;

public class BalloonLogic : MonoBehaviour {

    public GameConfig.BallonType CurrentBallonType = GameConfig.BallonType.ENMY;

    public void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(string.Format("tag {0}", other.gameObject.tag));

        if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.ENMY.ToString() && CurrentBallonType == GameConfig.BallonType.PLAYER)
        {
            SendMessageUpwards("BallonCollisionEnter", this.gameObject, SendMessageOptions.RequireReceiver);
        }

        if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString() && CurrentBallonType == GameConfig.BallonType.ENMY)
        {
            SendMessageUpwards("BallonCollisionEnter", this.gameObject, SendMessageOptions.RequireReceiver);
        }
    }




}
