﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class OperationCtrl : MonoBehaviour
{
    public Transform Hero;
    private bool MoveState { get; set; }
    private GameConfig.Direction DirHR { get; set; }
    private GameConfig.Direction DirVH { get; set; }

    private float drag;

    void Start()
    {
        Bpc = new BasePropertiyConfig
        {
            HRDrag = 120.2f,
            VRDrag = -1.65f,
            UpForce = 120f,
            Mass = 22.08f
        };


        drag = Hero.rigidbody2D.drag;
        Hero.rigidbody2D.mass = Bpc.Mass;

        DirVH = GameConfig.Direction.DOWN;
        DirHR = GameConfig.Direction.DOWN;
    }

    BasePropertiyConfig Bpc;


    void OnGUI() 
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            MoveState = true;
            Hero.rigidbody2D.drag = drag;
            DirVH = GameConfig.Direction.UP;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            Hero.rigidbody2D.drag = Bpc.VRDrag;
            DirVH = GameConfig.Direction.DOWN;
        }

    }

    public void OnPress(GameObject o)
    {
        Hero.rigidbody2D.drag = drag;

        MoveState = true;
        switch (o.name)
        {
            case "UP":
                DirVH = GameConfig.Direction.UP;
                break;
            case "LEFT":
                DirHR = GameConfig.Direction.LEFT;
                break;
            case "RIGHT":
                DirHR = GameConfig.Direction.RIGHT;
                break;
            default:
                break;
        }
    }

    public void OnRelease(GameObject o)
    {
        Hero.rigidbody2D.drag = Bpc.VRDrag;


        switch (o.name)
        {
            case "UP":
                DirVH = GameConfig.Direction.DOWN;
                break;
            case "LEFT":
                DirHR = GameConfig.Direction.DOWN;
                break;
            case "RIGHT":
                DirHR = GameConfig.Direction.DOWN;
                break;
            default:
                break;
        }

        if (DirHR == GameConfig.Direction.DOWN && DirVH == GameConfig.Direction.DOWN)
        {
            MoveState = false;
        }
    }

    private void SwitchDirection(GameConfig.Direction dir)
    {
        switch (dir)
        {
            case GameConfig.Direction.LEFT:
                if (Hero.localScale.x == -1)
                {
                    Hero.localScale = new Vector3(1, 1, 1);
                }
                break;
            case GameConfig.Direction.RIGHT:
                if (Hero.localScale.x == 1)
                {
                    Hero.localScale = new Vector3(-1, 1, 1);
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (MoveState)
        {
            SwitchDirection(DirHR);

            if (DirHR == GameConfig.Direction.DOWN)
            {
                if (DirVH != GameConfig.Direction.DOWN)
                {
                    Hero.rigidbody2D.AddForce(new Vector2(0, 1) * Bpc.UpForce);
                }
            }
            else
            {
                if (DirVH != GameConfig.Direction.DOWN)
                {
                    switch (DirHR)
                    {
                        case GameConfig.Direction.UP:
                            break;
                        case GameConfig.Direction.DOWN:
                            break;
                        case GameConfig.Direction.LEFT:

                            Hero.rigidbody2D.AddForce(new Vector2(-1, 1) * Bpc.UpForce);
                            break;
                        case GameConfig.Direction.RIGHT:
                            Hero.rigidbody2D.AddForce(new Vector2(1, 1) * Bpc.UpForce);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (DirHR)
                    {
                        case GameConfig.Direction.UP:
                            break;
                        case GameConfig.Direction.DOWN:
                            break;
                        case GameConfig.Direction.LEFT:
                            Hero.rigidbody2D.AddForce(new Vector2(-1, 0) * Bpc.HRDrag);
                            break;
                        case GameConfig.Direction.RIGHT:
                            Hero.rigidbody2D.AddForce(new Vector2(1, 0) * Bpc.HRDrag);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

   
        if (GameConfig.REEL_STATE)
        {
            Hero.rigidbody2D.velocity = new Vector2(Mathf.Clamp(Hero.rigidbody2D.velocity.x, -.9f - GameConfig.REEL_SPEED, .9f - GameConfig.REEL_SPEED), Mathf.Clamp(Hero.rigidbody2D.velocity.y, -20f, .6f));
        }
        else { Hero.rigidbody2D.velocity = new Vector2(Mathf.Clamp(Hero.rigidbody2D.velocity.x, -.9f, .9f), Mathf.Clamp(Hero.rigidbody2D.velocity.y, -20f, .6f)); }

        //Debug.Log(string.Format("Current SPEET : {0}", Hero.rigidbody2D.velocity));
    }
}