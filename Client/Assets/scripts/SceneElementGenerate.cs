﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

    
public class SceneElementGenerate : MonoBehaviour {

    public GameObject TemplatePrefab;

    //public List<UITexture> TextureList;



	// Use this for initialization
	void Start () {
        GameObject Temp = null;
        for (int i = 0; i < GameConfig.TestPage; i++)
        {
            Temp = GameObject.Instantiate(TemplatePrefab.gameObject) as GameObject;
            Temp.transform.parent = TemplatePrefab.transform.parent;
            Temp.SetActive(true);
            Temp.transform.localScale = Vector3.one;
            Temp.transform.localPosition = new Vector3(GameConfig.BackgroundImageWidth * i,0,0);
        }
	}

}
