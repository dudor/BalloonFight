﻿using UnityEngine;
using System.Collections;
using System;

public class testEnmyCtrl1 : MonoBehaviour
{
    private bool MoveState { get; set; }
    private Direction DirHR { get; set; }
    private Direction DirVH { get; set; }

    private float drag;

    public int HP = 3;
    public float MinimumHeight = -200f;
    public float MaxHeight = 220f;

    void Start()
    {

        Bpc = new BasePropertiyConfig
        {
            HRDrag = 59.2f,
            VRDrag = 1.65f,
            UpForce = 68.63f,
            Mass = 22.08f
        };


        drag = rigidbody2D.drag;
        rigidbody2D.mass = Bpc.Mass;

        DirVH = Direction.DOWN;
        DirHR = Direction.DOWN;
        MoveState = true;

    }

    BasePropertiyConfig Bpc;


    private void SwitchDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.LEFT:
                if (transform.localScale.x == -1)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
                break;
            case Direction.RIGHT:
                if (transform.localScale.x == 1)
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                }
                break;
            default:
                break;
        }
    }

    Direction GetDirection(Direction d) 
    {
        if (d == Direction.DOWN)
        {
            System.Random r = new System.Random();
            return r.Next(1, 3) == 1 ? Direction.LEFT : Direction.RIGHT;
        }
        return d == Direction.LEFT ? Direction.RIGHT : Direction.LEFT;
    }

    float totalTime;

    void Update()
    {
        if (MinimumHeight>transform.localPosition.y)
            DirVH = Direction.UP;
        if (MaxHeight < transform.localPosition.y)
            DirVH = Direction.DOWN;

        totalTime += Time.deltaTime;
        if (totalTime>3)
        {
            DirHR = GetDirection(DirHR);
            totalTime = 0;
        }

        if (MoveState)
        {
            SwitchDirection(DirHR);

            if (DirHR == Direction.DOWN)
            {
                if (DirVH != Direction.DOWN)
                {
                    rigidbody2D.AddForce(new Vector2(0, 1) * Bpc.UpForce);
                }
            }
            else
            {
                if (DirVH != Direction.DOWN)
                {
                    switch (DirHR)
                    {
                        case Direction.UP:
                            break;
                        case Direction.DOWN:
                            break;
                        case Direction.LEFT:

                            rigidbody2D.AddForce(new Vector2(-1, 1) * Bpc.UpForce);
                            break;
                        case Direction.RIGHT:
                            rigidbody2D.AddForce(new Vector2(1, 1) * Bpc.UpForce);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (DirHR)
                    {
                        case Direction.UP:
                            break;
                        case Direction.DOWN:
                            break;
                        case Direction.LEFT:
                            rigidbody2D.AddForce(new Vector2(-1, 0) * Bpc.HRDrag);
                            break;
                        case Direction.RIGHT:
                            rigidbody2D.AddForce(new Vector2(1, 0) * Bpc.HRDrag);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    public void BallonCollisionEnter(GameObject o) 
    {
        o.SetActive(false);

        HP -= 1;
        if (HP == 0)
        {
            gameObject.SetActive(false);
        }
    }



    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}