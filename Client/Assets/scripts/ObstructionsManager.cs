﻿using UnityEngine;
using System.Collections;

public class ObstructionsManager : MonoBehaviour {

    public GameObject TemplatePrefab;
	// Use this for initialization
	void Start () {
        GameObject Temp = null;
        for (int i = 1; i < GameConfig.TestPage; i++)
        {
            Temp = GameObject.Instantiate(TemplatePrefab.gameObject) as GameObject;
            Temp.transform.parent = TemplatePrefab.transform.parent;
            Temp.SetActive(true);
            Temp.name = string.Format("GROUP{0}", i);
            Temp.transform.localScale = Vector3.one;
            Temp.transform.localPosition = new Vector3(GameConfig.BackgroundImageWidth * i, 0, 0);
            if (GameConfig.TestPage==i+1)
            {
                //Temp.GetComponent<ObstructionsLogicTest>().Model = GameConfig.ObstructionModel.FINALLY;
                //Temp.GetComponent<ObstructionsLogicTest>().BossStageCollider.SetActive(true);
                //Temp.GetComponent<ObstructionsLogicTest>().GoldContrainer.SetActive(false);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
