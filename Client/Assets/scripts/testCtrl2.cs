﻿//using UnityEngine;
//using System.Collections;

//public class testCtrl2 : MonoBehaviour
//{
//    public UILabel labelA, labelB, labelC, labelD, labelDES;

//    public Transform Hero;
//    public bool MoveState { get; set; }
//    public DirectionDir DirHR { get; set; }
//    public DirectionDir DirVH { get; set; }

//    public UISlider Sd;
//    public UISlider DGVR;
//    public UISlider DGHR;
//    public UISlider DGMASSS;

//    private float drag;

//    void Start()
//    {
//        drag = Hero.rigidbody2D.drag;
//        DirVH = DirectionDir.DOWN;
//        DirHR = DirectionDir.DOWN;
//    }

//    private const string str = "换算公式 \n上升常量比例*200 = {0} \nVR  阻力比例*{4} = {1} \nHR  阻力比例*200 = {2} \n英雄质量比例*100 = {3}";

//    public void ValueChange(GameObject o)
//    {
//        switch (o.name)
//        {
//            case "A":
//                labelA.text =Sd.value.ToString();
//                break;
//            case "B":
//                labelB.text = DGVR.value.ToString();
//                break;
//            case "C":
//                labelC.text = DGHR.value.ToString();
//                break;
//            case "D":
//                Hero.rigidbody2D.mass = DGMASSS.value * 100;
//                labelD.text = DGMASSS.value.ToString();
//                break;
//            default:
//                break;
//        }

//        labelDES.text = string.Format(str, (Sd.value * 200), (DGVR.value * 5), (DGHR.value * 200), (DGMASSS.value * 100),drag);
//    }

//    public void OnPress(GameObject o)
//    {
//        Hero.rigidbody2D.drag = drag;

//        MoveState = true;
//        switch (o.name)
//        {
//            case "top":
//                DirVH = DirectionDir.UP;
//                break;
//            case "down":

//                break;
//            case "left":
//                DirHR = DirectionDir.LEFT;
//                break;
//            case "right":
//                DirHR = DirectionDir.RIGHT;
//                break;
//            default:
//                break;
//        }
//    }

//    public void OnRelease(GameObject o)
//    {
//        Hero.rigidbody2D.drag = drag * DGVR.value;




//        switch (o.name)
//        {
//            case "top":
//                DirVH = DirectionDir.DOWN;
//                break;
//            case "down":

//                break;
//            case "left":
//                DirHR = DirectionDir.DOWN;
//                break;
//            case "right":
//                DirHR = DirectionDir.DOWN;
//                break;
//            default:
//                break;
//        }

//        if (DirHR == DirectionDir.DOWN && DirVH == DirectionDir.DOWN)
//        {
//            MoveState = false;
//        }
//    }

//    private void SwitchDirection(DirectionDir dir)
//    {
//        switch (dir)
//        {
//            case DirectionDir.LEFT:
//                if (Hero.localScale.x == -1)
//                {
//                    Hero.localScale = new Vector3(1, 1, 1);
//                }
//                break;
//            case DirectionDir.RIGHT:
//                if (Hero.localScale.x == 1)
//                {
//                    Hero.localScale = new Vector3(-1, 1, 1);
//                }
//                break;
//            default:
//                break;
//        }
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (MoveState)
//        {
//            SwitchDirection(DirHR);

//            if (DirHR == DirectionDir.DOWN)
//            {
//                if (DirVH != DirectionDir.DOWN)
//                {
//                    Hero.rigidbody2D.AddForce(new Vector2(0, 1) * 200f * Sd.value);
//                }
//            }
//            else
//            {
//                if (DirVH != DirectionDir.DOWN)
//                {
//                    switch (DirHR)
//                    {
//                        case DirectionDir.UP:
//                            break;
//                        case DirectionDir.DOWN:
//                            break;
//                        case DirectionDir.LEFT:
//                            Hero.rigidbody2D.AddForce(new Vector2(-1, 1) * 200f * Sd.value);
//                            break;
//                        case DirectionDir.RIGHT:
//                            Hero.rigidbody2D.AddForce(new Vector2(1, 1) * 200f * Sd.value);
//                            break;
//                        default:
//                            break;
//                    }
//                }
//                else
//                {
//                    switch (DirHR)
//                    {
//                        case DirectionDir.UP:
//                            break;
//                        case DirectionDir.DOWN:
//                            break;
//                        case DirectionDir.LEFT:
//                            Hero.rigidbody2D.AddForce(new Vector2(-1, 0) * 200f * DGHR.value);
//                            break;
//                        case DirectionDir.RIGHT:
//                            Hero.rigidbody2D.AddForce(new Vector2(1, 0) * 200f * DGHR.value);
//                            break;
//                        default:
//                            break;
//                    }
//                }
//            }
//        }
//    }
//    public enum DirectionDir
//    {
//        UP,
//        DOWN,
//        LEFT,
//        RIGHT
//    }
//}


