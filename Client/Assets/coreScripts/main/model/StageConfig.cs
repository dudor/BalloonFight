﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Assets.coreScripts.main.model
{
    public class StageConfig : BaseConfig
    {
        public int BossId { get; set; }
        [JsonIgnore]
        public UnityEngine.Vector3 BossInitPosition { get { return new UnityEngine.Vector3(Boss_x, Boss_y, 0); } }

        [JsonIgnore]
        public UnityEngine.Vector3 HeroInitPosition { get { return new UnityEngine.Vector3(Hero_x, Hero_y, 0); } }
        public float Boss_x { get; set; }
        public float Boss_y { get; set; }
        public float Hero_x { get; set; }
        public float Hero_y { get; set; }
        public List<ReelInfo> ReelList { get; set; }
    }
}
