﻿using System;
using System.Collections.Generic;

namespace Assets.coreScripts.main.model
{
    public class ReelInfo:BaseConfig
    {
        //public string BackgroundName { get; set; }
        public bool IsBossReel { get; set; }
        public float Speed { get; set; }
    }
}
