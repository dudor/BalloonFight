﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.coreScripts.main.model
{
    public class User
    {
        public int Gold { get; set; }
        public int Crystal { get; set; }
        public int HeroId { get { return 1; } }

        public int HP = 3;
        public int MonstHP = 3;
        public int CurrentStateID { get { return 1001; } }
    }
}
