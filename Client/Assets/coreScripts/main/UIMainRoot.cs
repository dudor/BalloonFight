﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.context.impl;

namespace Assets.coreScripts.main
{
    public class UIMainRoot:ContextView
    {
        void Awake() 
        {
            context = new UIMainContext();
        }
    }
}
