﻿using System;
using System.Collections.Generic;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace Assets.coreScripts.fighting.model
{
    public class CustomHitEventData : CustomEventData
    {
        public float force { get; set; }
        public GameConfig.Direction forceDir { get; set; }
        public GameObject targetObj { get; set; }
        public GameConfig.ObstructionModel hitModel { get; set; }
        
    }
}
