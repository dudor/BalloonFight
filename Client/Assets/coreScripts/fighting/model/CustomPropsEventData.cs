﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.coreScripts.fighting.model
{
    public class CustomPropsEventData:CustomEventData
    {
        public GameConfig.PropsState propStatus { get; set; }
        public bool IsPause { get; set; }
        public UnityEngine.UI.Image img { get; set; }

    }
}
