﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.fighting.model;
using UnityEngine;

namespace Assets.coreScripts.fighting.common
{
    public class FightingCommon
    {
        public void SwitchDir(CustomOperationEventData data)
        {
            
            if (data.str.ToUpper() == GameConfig.Direction.LEFT.ToString())
            {
                data.dir = GameConfig.Direction.LEFT;
            }
            if (data.str.ToUpper() == GameConfig.Direction.RIGHT.ToString())
            {
                data.dir = GameConfig.Direction.RIGHT;
            }
            if (data.str.ToUpper() == GameConfig.Direction.UP.ToString())
            {
                data.dir = GameConfig.Direction.UP;
            }
            if (data.str.ToUpper() == GameConfig.Direction.DOWN.ToString())
            {
                data.dir = GameConfig.Direction.DOWN;
            }
        }

        public GameConfig.Direction RandomDirection(GameConfig.Direction d)
        {
            if (d == GameConfig.Direction.DOWN)
            {
                System.Random r = new System.Random();
                return r.Next(1, 3) == 1 ? GameConfig.Direction.LEFT : GameConfig.Direction.RIGHT;
            }
            return d == GameConfig.Direction.LEFT ? GameConfig.Direction.RIGHT : GameConfig.Direction.LEFT;
        }

        public void KillAllballoon(UnityEngine.Transform t)
        {
            UnityEngine.Transform tempT = null;

            for (int i = 1; i <= (int)GameConfig.MAX_BALLON; i++)
            {
                tempT = t.FindChild(string.Format("Ballon{0}", i));
                if (tempT)
                {
                    tempT.gameObject.SetActive(false);
                }
            }
        }

        public void ActiveAllballoon(UnityEngine.Transform t)
        {
            UnityEngine.Transform tempT = null;

            foreach (Transform item in t)
            {
                if (item.name.StartsWith("Ballon"))
                {
                    item.gameObject.SetActive(true);
                }
            }
        }

        public void AddDirctionForceToRigibody2D(GameObject o,GameConfig.Direction dir,float force) 
        {
            if (!o.rigidbody2D)
            {
                Debug.Log(string.Format("{0} haven't rigidbody2D",o.name));
                return;
            }
            Debug.Log(force + "Force");
            switch (dir)
            {
                case GameConfig.Direction.UP:
                   
                    o.rigidbody2D.AddForce(Vector2.up * force, ForceMode2D.Impulse);
                    break;
                case GameConfig.Direction.DOWN:
                    o.rigidbody2D.AddForce(-Vector2.up * force, ForceMode2D.Impulse);
                    break;
                case GameConfig.Direction.LEFT:
                    o.rigidbody2D.AddForce(-Vector2.right * force, ForceMode2D.Impulse);
                    break;
                case GameConfig.Direction.RIGHT:
                    o.rigidbody2D.AddForce(Vector2.right * force, ForceMode2D.Impulse);
                    break;
                default:
                    break;
            }
        }


    }
}
