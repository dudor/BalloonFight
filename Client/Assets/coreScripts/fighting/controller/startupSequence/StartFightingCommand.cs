﻿using System;
using UnityEngine;

using strange.extensions.command.impl;
using strange.extensions.command.api;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using Assets.coreScripts.fighting.util;
using Assets.coreScripts.common;

namespace Assets.coreScripts.fighting.controller.startupSequence
{
    public class StartFightingCommand : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        //[Inject(ContextKeys.CONTEXT)]
        //public IContext context { get; set; }

        public override void Execute()
        {
            IGameTimer timer = contextView.AddComponent<GameLoop>();
            injectionBinder.Bind<IGameTimer>().ToValue(timer);

            injectionBinder.GetInstance<DataBaseCommon>().Init();
            //dispatcher.Dispatch(GameConfig.CoreEvent.GAME_START);
            //dispatcher.Dispatch(GameConfig.ReelState.START);
            dispatcher.Dispatch(GameConfig.CoreEvent.GAME_START);
            dispatcher.Dispatch(GameConfig.ReelState.START);

            Debug.Log("StartFightingCommand Execute");
        }

    }
}
