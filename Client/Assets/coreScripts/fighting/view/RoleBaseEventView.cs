﻿using System;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class RoleBaseEventView : View
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }
        protected bool MoveState { get; set; }

        protected GameConfig.Direction DirHR { get; set; }
        protected GameConfig.Direction DirVH { get; set; }
        protected BaseRolePropertiy bpc { get; set; }


        public virtual void Init(BaseRolePropertiy Bpc) 
        {
            bpc = Bpc;
            transform.rigidbody2D.drag = Bpc.OriginalDrag;
            transform.rigidbody2D.mass = Bpc.OriginalMass;

            DirVH = GameConfig.Direction.DOWN;
            DirHR = GameConfig.Direction.DOWN;
        }



        public void MoveToDirection(IEvent e)
        {
            GameConfig.Direction dir = (GameConfig.Direction)e.data;
            transform.rigidbody2D.drag = bpc.OriginalDrag;

            if (dir == GameConfig.Direction.LEFT || dir == GameConfig.Direction.RIGHT)
            {
                DirHR = dir;
            }
            else 
            {
                DirVH = dir;
            }
        }

        public void CancelToDirection(IEvent e)
        {
            GameConfig.Direction dir = (GameConfig.Direction)e.data;
            transform.rigidbody2D.drag = bpc.VRDrag;

            if (dir == GameConfig.Direction.LEFT || dir == GameConfig.Direction.RIGHT)
            {
                DirHR = GameConfig.Direction.DOWN;
            }
            else
            {
                DirVH = GameConfig.Direction.DOWN;
            }

            //if (DirHR == GameConfig.Direction.DOWN && DirVH == GameConfig.Direction.DOWN)
            //{
            //    MoveState = false;
            //}
        }



        public virtual void GameUpdate()
        {

            //if (MoveState)
            //{
                SwitchDirection(DirHR);

                if (DirHR == GameConfig.Direction.DOWN)
                {
                    if (DirVH != GameConfig.Direction.DOWN)
                    {
                        transform.rigidbody2D.AddForce(new Vector2(0, 1) * bpc.UpForce);
                    }
                }
                else
                {
                    if (DirVH != GameConfig.Direction.DOWN)
                    {
                        switch (DirHR)
                        {
                            case GameConfig.Direction.UP:
                                break;
                            case GameConfig.Direction.DOWN:
                                break;
                            case GameConfig.Direction.LEFT:

                                transform.rigidbody2D.AddForce(new Vector2(-1, 1) * bpc.UpForce);
                                break;
                            case GameConfig.Direction.RIGHT:
                                transform.rigidbody2D.AddForce(new Vector2(1, 1) * bpc.UpForce);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (DirHR)
                        {
                            case GameConfig.Direction.UP:
                                break;
                            case GameConfig.Direction.DOWN:
                                break;
                            case GameConfig.Direction.LEFT:
                                transform.rigidbody2D.AddForce(new Vector2(-1, 0) * bpc.HRDrag);
                                break;
                            case GameConfig.Direction.RIGHT:
                                transform.rigidbody2D.AddForce(new Vector2(1, 0) * bpc.HRDrag);
                                break;
                            default:
                                break;
                        }
                    }
                //}
            }


            if (GameConfig.REEL_STATE)
            {
                transform.rigidbody2D.velocity = new Vector2(Mathf.Clamp(transform.rigidbody2D.velocity.x, bpc.MinVoc.x - GameConfig.REEL_SPEED, bpc.MaxVoc.x - GameConfig.REEL_SPEED), Mathf.Clamp(transform.rigidbody2D.velocity.y, bpc.MinVoc.y, bpc.MaxVoc.y));
            }
            else { transform.rigidbody2D.velocity = new Vector2(Mathf.Clamp(transform.rigidbody2D.velocity.x, bpc.MinVoc.x, bpc.MaxVoc.x), Mathf.Clamp(transform.rigidbody2D.velocity.y, bpc.MinVoc.y, bpc.MaxVoc.y)); }
        }

        protected void SwitchDirection(GameConfig.Direction dir)
        {
            switch (dir)
            {
                case GameConfig.Direction.LEFT:
                    if (transform.localScale.x == -1)
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                    }
                    break;
                case GameConfig.Direction.RIGHT:
                    if (transform.localScale.x == 1)
                    {
                        transform.localScale = new Vector3(-1, 1, 1);
                    }
                    break;
                default:
                    break;
            }
        }


   

    }
}
