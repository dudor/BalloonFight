﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.coreScripts.fighting.view
{
    public class PropsView : FightingBaseView
    {
        public Image titleImg;


        public override void Init()
        {
            base.Init();
        }

        public void ReStart()
        {
            CustomPropsEventData data = new CustomPropsEventData();
            data.propStatus = GameConfig.PropsState.RESTART;
            data.type = GameConfig.PropsState.PROPS_COMMAND;
            data.currentObj = gameObject;
            data.img = titleImg; 
            dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, data);
        }

        public void Pause_Release()
        {
            CustomPropsEventData data = new CustomPropsEventData();
            data.propStatus = GameConfig.PropsState.PAUSE_RELEASE;
            data.type = GameConfig.PropsState.PROPS_COMMAND;
            data.currentObj = gameObject;
            data.img = titleImg; 
            dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND,data);
        }

        public void OnPause()
        {
            CustomPropsEventData data = new CustomPropsEventData();
            data.propStatus = GameConfig.PropsState.OnPause;
            data.type = GameConfig.PropsState.PROPS_COMMAND;
            data.currentObj = gameObject;
            data.IsPause = true;
            data.img = titleImg; 
            dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND,data);
        }


    }
}
