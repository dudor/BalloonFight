﻿using System;
using System.Collections.Generic;

using strange.extensions.mediation.impl;
using strange.extensions.mediation.api;

using Assets.coreScripts.main.model;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.common;

namespace Assets.coreScripts.fighting.view
{
    public class PlayerMediator : FightingBaseEventMediator
    {
        [Inject]
        public PlayerView view { get; set; }

        [Inject]
        public User user { get; set; }
        [Inject]
        public DataBaseCommon database { get; set; }
        [Inject]
        public common.FightingCommon common { get; set; }

        public override void OnRegister()
        {
            Debug.Log("PlayerMediator.OnRegister");
            //var Bpc = new HeroConfig()
            // {
            //     HRDrag = 120.2f,
            //     VRDrag = -1.65f,
            //     UpForce = 120f,
            //     OriginalMass = 22.08f,
            //     OriginalDrag = 5f,
            //     OriginalID = 0,
            //     Min_x = -.9f,
            //     Min_y = -20f,
            //     Max_x = .9f,
            //     Max_y = .6f,
            // };

            //view.Init(Bpc);
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }


        protected override void UpdateListeners(bool enable)
        {
            //view.dispatcher.UpdateListener(enable, GameConfig.RoleEvent.PLAYER_GETHIT, OnHeroGetHit);
            dispatcher.UpdateListener(enable, GameConfig.RoleEvent.PLAYER_MOVE, view.MoveToDirection);
            dispatcher.UpdateListener(enable, GameConfig.RoleEvent.PLAYER_CANCEL_MOVE, view.CancelToDirection);
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_UPDATE, view.GameUpdate);
            dispatcher.UpdateListener(enable, GameConfig.PropsState.OnPause, OnPause);
            dispatcher.UpdateListener(enable, GameConfig.PropsState.PAUSE_RELEASE, OnPause_Release);
            base.UpdateListeners(enable);
        }

        private void OnPause(IEvent payload)
        {
            gameObject.rigidbody2D.isKinematic =true;
        }
        private void OnPause_Release(IEvent payload)
        {
            gameObject.rigidbody2D.isKinematic = false;
        }


        //private void OnHeroGetHit(IEvent e)
        //{
        //    var data = e as CustomHitEventData;
        //    data.type = GameConfig.PlayerState.GET_HIT;
        //    dispatcher.Dispatch(GameConfig.PlayerState.GET_HIT, data);
        //}

        protected override void InitData()
        {
            var config = database.GetConfigByID(user.HeroId, database.HeroConfigList);
            var stageConfig = database.GetConfigByID(user.CurrentStateID, database.StageConfigList);
            view.Init(config);
            user.HP = 3;
            rigidbody2D.isKinematic = false;
            view.transform.localPosition = stageConfig.HeroInitPosition;
        }

        protected override void OnGameRestart()
        {
            InitData();
            gameObject.rigidbody2D.isKinematic = false;
            common.ActiveAllballoon(view.transform);
            base.OnGameRestart();
        }

    }
}
