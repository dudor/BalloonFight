﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class ReelView : FightingBaseView
    {
        public Transform targetContainer;

        internal StageConfig config;

        private List<ReelInfoExtension> infoList;
        private ReelInfoExtension tempInfo { get; set; }
        private GameObject tempSourceObj { get; set; }
        private GameObject tempPrefab { get; set; }

        //private float maxDistance { get; set; }


        public void Init(StageConfig cfg)
        {
            config = cfg;

            if (infoList == null)
            {
                infoList = new List<ReelInfoExtension>();

                for (int i = 0; i < config.ReelList.Count; i++)
                {
                    tempSourceObj = Resources.Load<GameObject>(string.Format("Prefab/CoreFighting/StageElement/{0}", cfg.ReelList[i].ID));

                    tempInfo = new ReelInfoExtension();
                    tempInfo.ID = config.ReelList[i].ID;
                    tempInfo.IsBossReel = config.ReelList[i].IsBossReel;
                    tempInfo.obj = GameObject.Instantiate(tempSourceObj) as GameObject;
                    tempInfo.obj.transform.parent = targetContainer;
                    tempInfo.obj.transform.localPosition = new Vector2(i * GameConfig.BackgroundImageWidth, 0);
                    tempInfo.obj.SetActive(false);
                    tempInfo.hasRender = false;
                    tempInfo.Speed = config.ReelList[i].Speed;

                    //if (i == config.ReelList.Count - 1)
                    //{
                    //    tempInfo.obj.transform.FindChild("gold").gameObject.SetActive(false);
                    //}

                    infoList.Add(tempInfo);
                }
            }

            infoList[infoList.Count - 1].obj.transform.FindChild("gold").gameObject.SetActive(false);

            if (infoList.Count == 1)
            {
                dispatcher.Dispatch(GameConfig.ReelState.STOP);
                infoList[0].obj.SetActive(true);
                complete = true;
                return;
            }

            //SET DEFAULT
            renderIndex = 2;
            infoList[0].obj.SetActive(true);
            infoList[1].obj.SetActive(true);
            maxDistance = infoList[infoList.Count - 1].obj.transform.localPosition.x;

            rigidbody2D.velocity = new Vector2(infoList[0].Speed, 0);
        }

        public int currentIndex { get; set; }
        public int renderIndex { get; set; }
        public bool renderOnce { get; set; }
        public bool complete { get; set; }
        public float maxDistance { get; set; }

        public override void GameUpdate()
        {
            if (complete)
                return;

            if (transform.localPosition.x >= maxDistance)
            {
                for (int i = 0; i < infoList.Count - 1; i++)
                {
                    infoList[i].obj.SetActive(false);
                }
                transform.rigidbody2D.velocity = Vector2.zero;
                complete = true;
                dispatcher.Dispatch(GameConfig.ReelState.STOP);
                return;
            }

            if (Mathf.Round(transform.localPosition.x) % Mathf.Round(GameConfig.BackgroundImageWidth) == 0)
            {
                if (renderOnce)
                    return;
                if (++currentIndex == infoList.Count)
                {
                    transform.rigidbody2D.velocity = Vector2.zero;
                    return;
                }

                transform.rigidbody2D.velocity = new Vector2(infoList[currentIndex].Speed, 0);


                renderOnce = true;
                if (infoList.Count > renderIndex)
                {
                    infoList[renderIndex].obj.SetActive(true);
                    if ((renderIndex - 2) >= 0 && renderIndex != 2)
                    {
                        infoList[renderIndex - 3].obj.SetActive(false);
                    }
                    renderIndex++;
                }
            }
            else
            {
                renderOnce = false;
            }
        }

        public void Reset()
        {
            complete = false;
            renderOnce = false;
            currentIndex = 0;
            transform.rigidbody2D.velocity = Vector2.zero;
            transform.localPosition = new Vector3(0,0,-10);
            infoList.ForEach(x=>x.obj.SetActiveRecursively(true));
            Init(config);
        }

        public void Stop()
        {
            rigidbody2D.velocity = Vector2.zero;
        }

        public Vector2 currentSpeed { get; set; }
        public void PauseReel(IEvent e)
        {
            CustomPropsEventData data = e as CustomPropsEventData;
            if (data.IsPause)
            {
                currentSpeed = transform.rigidbody2D.velocity;
            }
            else 
            {
                transform.rigidbody2D.velocity = currentSpeed;
            }
        }

    }
}
