﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class EnemyMediator :FightingBaseEventMediator
    {
        [Inject]
        public EnemyView view { get; set; }

        [Inject]
        public User user { get; set; }
        [Inject]
        public DataBaseCommon database { get; set; }
        [Inject]
        public common.FightingCommon common { get; set; }

        public override void OnRegister()
        {
            //MonsterConfig bpc = new MonsterConfig
            //{
            //    HRDrag = 120.2f,
            //    VRDrag = -1.65f,
            //    UpForce = 120f,
            //    OriginalMass = 22.08f,
            //    OriginalDrag = 5f,
            //    OriginalID = 0,
            //    Min_x = -.9f,
            //    Min_y = -20f,
            //    Max_x = .9f,
            //    Max_y = .6f,
            //    AI_Frequency = 3f,
            //};
            //view.Init(bpc);
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        protected override void UpdateListeners(bool enable)
        {
 
            view.dispatcher.UpdateListener(enable, GameConfig.RoleEvent.ENEMY_PRE_AI, PreAI);
            view.dispatcher.UpdateListener(enable, GameConfig.RoleEvent.ENEMY_GETHIT, OnEnemyGetHit);

            dispatcher.UpdateListener(enable,GameConfig.RoleEvent.SHOW_ENEMY,ShowEnemy);
            dispatcher.UpdateListener(enable, GameConfig.RoleEvent.EMEMY_CANCEL_MOVE, view.CancelToDirection);
            dispatcher.UpdateListener(enable, GameConfig.RoleEvent.ENEMY_MOVE, view.MoveToDirection);
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_UPDATE, view.GameUpdate);

            dispatcher.UpdateListener(enable, GameConfig.PropsState.OnPause, OnPause);
            dispatcher.UpdateListener(enable, GameConfig.PropsState.PAUSE_RELEASE, OnPause_Release);

            base.UpdateListeners(enable);
        }

        private void OnPause(IEvent payload)
        {
            gameObject.rigidbody2D.isKinematic = true;
        }
        private void OnPause_Release(IEvent payload)
        {
            gameObject.rigidbody2D.isKinematic = false;
        }

        private void ShowEnemy(IEvent e) 
        {
            CustomAIEventData data = e as CustomAIEventData;
            view.ShowEnemy(data.enemyLevel);
        }

        protected override void InitData()
        {
            var stageConfig = database.GetConfigByID(user.CurrentStateID, database.StageConfigList);
            var bossConfig = database.GetConfigByID(stageConfig.BossId, database.MonsterConfigList);
            view.transform.localPosition = stageConfig.BossInitPosition;
            rigidbody2D.isKinematic = false;
            view.Init(bossConfig);
        }

        private void OnEnemyGetHit(IEvent e)
        {
            CustomEventData data = e as CustomHitEventData;
            data.type = GameConfig.EnemyState.GET_HIT;

            dispatcher.Dispatch(GameConfig.EnemyState.GET_HIT, data);
        }


        protected override void OnGameOver()
        {
            rigidbody2D.isKinematic = true;
            base.OnGameOver();
        }

        protected override void OnGameRestart()
        {
            common.ActiveAllballoon(view.transform);
            view.gameObject.SetActive(false);
            rigidbody2D.isKinematic = false;
            user.MonstHP = 3;
            InitData();
            base.OnGameRestart();
        }

        private CustomAIEventData GetEnemyInfo() 
        {
            CustomAIEventData cd = new CustomAIEventData
            {
                currentObj = view.gameObject,
                enemyMinFly = view.minimumHeight,
                enemyMaxFly = view.maxHeight,
                enemyFlyHRDir = view.enemyHR,
                enemyFlyVHDir = view.enemyVH,
                type = GameConfig.RoleEvent.ENEMY_AI,
            };
           return cd;
        }

        private void PreAI()
        {
            view.totalTime += Time.deltaTime;
            if (view.totalTime >= view.monstConfig.AI_Frequency)
            {
                view.totalTime = 0;
                CustomAIEventData cd = GetEnemyInfo();
                cd.ai = GameConfig.AIEventType.AI_LOOP;
                dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_AI, cd);
            }
            else
            {
                if (view.minimumHeight > view.transform.localPosition.y || view.maxHeight < view.transform.localPosition.y)
                {
                    CustomAIEventData cd = GetEnemyInfo();
                    cd.ai = GameConfig.AIEventType.AI_FORCE;
                    dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_AI, cd);
                }
            }
        }

    }
}
