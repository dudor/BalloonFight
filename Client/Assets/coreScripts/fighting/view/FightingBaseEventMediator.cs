﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.fighting.view
{
    public class FightingBaseEventMediator:Mediator
    {
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        protected virtual void UpdateListeners(bool enable)
        {
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_START, InitData);
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_OVER, OnGameOver);
            dispatcher.AddListener(GameConfig.CoreEvent.GAME_RESTART, OnGameRestart);
        }

        protected virtual void OnGameRestart() { UpdateListeners(true); }

        protected virtual void OnGameOver() { UpdateListeners(false); }

        protected virtual void InitData() { }

    }
}
