﻿using System;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using strange.extensions.mediation.api;
using UnityEngine;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using Assets.coreScripts.fighting.model;

namespace Assets.coreScripts.fighting.view
{
    public class PlayerView : RoleBaseEventView
    {
        public override void Init(BaseRolePropertiy Bpc)
        {
            base.Init(Bpc);
        }

        public bool grounded { get; set; }

        public override void GameUpdate()
        {
            //grounded = Physics2D.Linecast(transform.localPosition, transform.localPosition - new Vector3(0, -1, 0), 1 << LayerMask.NameToLayer("Water"));
            //Debug.Log(grounded.ToString());
            if (base.bpc == null)
                return;

            SwitchDirection(DirHR);

            if (DirHR == GameConfig.Direction.DOWN)
            {
                if (DirVH != GameConfig.Direction.DOWN)
                {
                    transform.rigidbody2D.AddForce(new Vector2(0, 1) * bpc.UpForce);
                }
            }
            else
            {
                if (DirVH != GameConfig.Direction.DOWN)
                {
                    switch (DirHR)
                    {
                        case GameConfig.Direction.UP:
                            break;
                        case GameConfig.Direction.DOWN:
                            break;
                        case GameConfig.Direction.LEFT:

                            transform.rigidbody2D.AddForce(new Vector2(-1, 1) * bpc.UpForce);
                            break;
                        case GameConfig.Direction.RIGHT:
                            transform.rigidbody2D.AddForce(new Vector2(1, 1) * bpc.UpForce);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (CheckPlantform())
                    {
                        switch (DirHR)
                        {
                            case GameConfig.Direction.UP:
                                break;
                            case GameConfig.Direction.DOWN:
                                break;
                            case GameConfig.Direction.LEFT:
                                transform.rigidbody2D.AddForce(new Vector2(-1, 0) * bpc.HRDrag);
                                break;
                            case GameConfig.Direction.RIGHT:
                                transform.rigidbody2D.AddForce(new Vector2(1, 0) * bpc.HRDrag);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }


            if (GameConfig.REEL_STATE)
            {
                transform.rigidbody2D.velocity = new Vector2(Mathf.Clamp(transform.rigidbody2D.velocity.x, bpc.MinVoc.x - GameConfig.REEL_SPEED, bpc.MaxVoc.x - GameConfig.REEL_SPEED), Mathf.Clamp(transform.rigidbody2D.velocity.y, bpc.MinVoc.y, bpc.MaxVoc.y));
            }
            else { transform.rigidbody2D.velocity = new Vector2(Mathf.Clamp(transform.rigidbody2D.velocity.x, bpc.MinVoc.x, bpc.MaxVoc.x), Mathf.Clamp(transform.rigidbody2D.velocity.y, bpc.MinVoc.y, bpc.MaxVoc.y)); }
        }

        private bool CheckPlantform() 
        {
            //Debug.Log(Camera.main.WorldToViewportPoint(transform.localPosition));
            //Debug.DrawLine(Camera.main.ViewportToScreenPoint(transform.localPosition), Camera.main.ViewportToScreenPoint(transform.localPosition) + new Vector3(0, -1f, 0), Color.red, 99);
            return grounded;   
        }

        public void OnCollisionStay2D(Collision2D coll)
        {
            grounded = true;
        }

        public void OnCollisionExit2D(Collision2D coll)
        {
            grounded = false;
        }




    }
}
