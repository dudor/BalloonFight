﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class EnemyView : RoleBaseEventView
    {
        [HideInInspector]
        public float totalTime;

        public int HP = 3;
        public float minimumHeight = -200f;
        public float maxHeight = 220f;
        public GameConfig.EnemyLevel enemyLevel = GameConfig.EnemyLevel.NORMAL;

        public MonsterConfig monstConfig{get;set;}
        public GameConfig.Direction enemyHR { get { return base.DirHR; } }
        public GameConfig.Direction enemyVH { get { return base.DirVH; } }

        public override void Init(BaseRolePropertiy b)
        {
            monstConfig = b as MonsterConfig;
            base.Init(b);
        }

        public override void GameUpdate()
        {
            if (monstConfig == null)
                return;
            dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_PRE_AI);
            base.GameUpdate();
        }

        public void ShowEnemy(GameConfig.EnemyLevel e) 
        {
            if (e== enemyLevel)
            {
                gameObject.SetActive(true);
            }
        }

        //private void BallonTrrigerEnter(IEvent e)
        //{
        //    CustomHitEventData data = e as CustomHitEventData;

        //    data.type = GameConfig.RoleEvent.ENEMY_GETHIT;

        //    dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_GETHIT, data);
        //}

        //public void OnEnemyDie(IEvent e)
        //{
        //    CustomHitEventData other = e as CustomHitEventData;

        //    other.type = GameConfig.RoleEvent.ENEMY_GETHIT;

        //    dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_GETHIT, other);
        //}
    }
}
