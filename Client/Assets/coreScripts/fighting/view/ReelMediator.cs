﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class ReelMediator :FightingBaseEventMediator
    {
        [Inject]
        public ReelView view { get; set; }

        [Inject]
        public User user { get; set; }

        [Inject]
        public DataBaseCommon database { get; set; }

        public override void OnRegister()
        {
            //view.Init(database.GetStageConfigByID(user.CurrentStateID));
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            UpdateListeners(false);
        }
        protected override void UpdateListeners(bool enable)
        {
            view.dispatcher.UpdateListener(enable,GameConfig.ReelState.STOP,StopReel);
            dispatcher.UpdateListener(enable, GameConfig.ReelState.START, StartReel);
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_UPDATE, view.GameUpdate);

            dispatcher.UpdateListener(enable, GameConfig.PropsState.OnPause, OnPause);
            dispatcher.UpdateListener(enable, GameConfig.PropsState.PAUSE_RELEASE, OnPause_Release);
            base.UpdateListeners(enable);
        }

        public Vector2 v { get; set; }
        private void OnPause(IEvent payload)
        {
            v = view.rigidbody2D.velocity;
            view.rigidbody2D.velocity = Vector2.zero;
        }
        private void OnPause_Release(IEvent payload)
        {
            view.rigidbody2D.velocity = v;
        }

        private void StartReel() 
        {
            view.Init(database.GetConfigByID(user.CurrentStateID,database.StageConfigList));
        }

        private void StopReel() 
        {
            dispatcher.Dispatch(GameConfig.RoleEvent.SHOW_ENEMY, new CustomAIEventData { enemyLevel = GameConfig.EnemyLevel.BOSS, type = GameConfig.RoleEvent.SHOW_ENEMY });
            view.Stop();
        }

        protected override void OnGameOver()
        {
            view.Stop();
            base.OnGameOver();
        }

        protected override void OnGameRestart()
        {
            view.Reset();
            base.OnGameRestart();
        }


    }
}
